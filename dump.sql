-- MySQL dump 10.14  Distrib 5.5.56-MariaDB, for Linux (x86_64)
--
-- Host: localhost    Database: mojazguba
-- ------------------------------------------------------
-- Server version	5.5.56-MariaDB

/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8 */;
/*!40103 SET @OLD_TIME_ZONE=@@TIME_ZONE */;
/*!40103 SET TIME_ZONE='+00:00' */;
/*!40014 SET @OLD_UNIQUE_CHECKS=@@UNIQUE_CHECKS, UNIQUE_CHECKS=0 */;
/*!40014 SET @OLD_FOREIGN_KEY_CHECKS=@@FOREIGN_KEY_CHECKS, FOREIGN_KEY_CHECKS=0 */;
/*!40101 SET @OLD_SQL_MODE=@@SQL_MODE, SQL_MODE='NO_AUTO_VALUE_ON_ZERO' */;
/*!40111 SET @OLD_SQL_NOTES=@@SQL_NOTES, SQL_NOTES=0 */;

--
-- Table structure for table `localization`
--

DROP TABLE IF EXISTS `localization`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `localization` (
  `time` datetime NOT NULL,
  `transmitter_id` char(6) NOT NULL,
  `latitude` float DEFAULT NULL,
  `longtitude` float DEFAULT NULL,
  `satelites` tinyint(3) unsigned DEFAULT NULL,
  `latency` tinyint(3) unsigned DEFAULT NULL,
  PRIMARY KEY (`time`,`transmitter_id`),
  KEY `transmitter_id` (`transmitter_id`),
  CONSTRAINT `localization_ibfk_1` FOREIGN KEY (`transmitter_id`) REFERENCES `transmitter` (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `localization`
--

LOCK TABLES `localization` WRITE;
/*!40000 ALTER TABLE `localization` DISABLE KEYS */;
INSERT INTO `localization` VALUES ('2018-03-19 09:00:00','qwerty',0,0,0,185),('2018-03-19 09:19:34','qwerty',0,0,0,64),('2018-03-19 10:00:00','qwerty',0,0,0,255);
/*!40000 ALTER TABLE `localization` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `transmitter`
--

DROP TABLE IF EXISTS `transmitter`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `transmitter` (
  `id` char(6) NOT NULL,
  `code` char(6) NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `transmitter`
--

LOCK TABLES `transmitter` WRITE;
/*!40000 ALTER TABLE `transmitter` DISABLE KEYS */;
INSERT INTO `transmitter` VALUES ('qwerty','Admin1');
/*!40000 ALTER TABLE `transmitter` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `user`
--

DROP TABLE IF EXISTS `user`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `user` (
  `id` bigint(20) NOT NULL AUTO_INCREMENT,
  `username` varchar(255) NOT NULL,
  `password` varchar(511) NOT NULL,
  `mail` varchar(255) NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=5 DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `user`
--

LOCK TABLES `user` WRITE;
/*!40000 ALTER TABLE `user` DISABLE KEYS */;
INSERT INTO `user` VALUES (1,'Dove6','$2y$10$UzQtHHD.BN511R8Wl3Wv/u0aiT5snyn2dRyzD6B.OKL3Pnk8tcgK.','45@gm.pl'),(2,'user','$2y$10$u9C/VvINzedlBHuj6g499Og6KHT/bN6l4m6Ii7j8cmn9CnQbInrWm','mail@mail.com'),(3,'Kuba','$2y$10$iFN/JCxjJ0ZmEgraV6Vs6uATCDC8yTl4jdeAOvaCz8S73ImJKsTfK','kuba@towarek.pl'),(4,'Dawid6','$2y$10$7m31IbmtgVJh0Jx4ykpJFucb/FTmiidO2EHCmMZAkcVFZVDfgO8HO','u');
/*!40000 ALTER TABLE `user` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `user_transmitter`
--

DROP TABLE IF EXISTS `user_transmitter`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `user_transmitter` (
  `id` bigint(20) NOT NULL AUTO_INCREMENT,
  `owner_id` bigint(20) NOT NULL,
  `transmitter_id` char(6) NOT NULL,
  PRIMARY KEY (`id`),
  KEY `owner_id` (`owner_id`),
  KEY `transmitter_id` (`transmitter_id`),
  CONSTRAINT `user_transmitter_ibfk_1` FOREIGN KEY (`owner_id`) REFERENCES `user` (`id`),
  CONSTRAINT `user_transmitter_ibfk_2` FOREIGN KEY (`transmitter_id`) REFERENCES `transmitter` (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=7 DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `user_transmitter`
--

LOCK TABLES `user_transmitter` WRITE;
/*!40000 ALTER TABLE `user_transmitter` DISABLE KEYS */;
INSERT INTO `user_transmitter` VALUES (2,3,'qwerty'),(5,1,'qwerty');
/*!40000 ALTER TABLE `user_transmitter` ENABLE KEYS */;
UNLOCK TABLES;
/*!40103 SET TIME_ZONE=@OLD_TIME_ZONE */;

/*!40101 SET SQL_MODE=@OLD_SQL_MODE */;
/*!40014 SET FOREIGN_KEY_CHECKS=@OLD_FOREIGN_KEY_CHECKS */;
/*!40014 SET UNIQUE_CHECKS=@OLD_UNIQUE_CHECKS */;
/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
/*!40111 SET SQL_NOTES=@OLD_SQL_NOTES */;

-- Dump completed on 2018-03-21 12:56:19
