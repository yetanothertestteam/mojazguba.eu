<?php
	/* 	PSEUDOWYJĄTKI UŻYTE W KODZIE ($exception):
		"MISSING_USERNAME" - niepodana nazwa użytkownika //do odsiania przez js
		"MISSING_PASSWORD" - niepodane hasło //do odsiania przez js
		"INVALID_PASSWORD" - nieprawidłowe hasło (logowanie)
		"MISSING_EMAIL" - niepodany adres email //do odsiania przez js
		"MISSING_ACCOUNT" - konto z podaną nazwą użytkownika / adresem email nie istnieje (logowanie)
		"USERNAME_TAKEN" - nazwa użytkownika już zajęta (rejestracja)
		"EMAIL_TAKEN" - adres email już zajęty (rejestracja)
	*/
	header("Cache-Control: no-cache");
	header("Pragma: no-cache");
	session_start();
	if (!isset($_SESSION["username"])) {
		if ($_SERVER["REQUEST_METHOD"] == "POST") {
			if (isset($_POST["username"]) && isset($_POST["password"]) && isset($_POST["action"])) {
				if ($_POST["username"] != "" && $_POST["password"] != "" && $_POST["action"] != "") {

					$dsn = "mysql:host=localhost;dbname=mojazguba;charset=utf8mb4";
					$opt = [
						PDO::ATTR_ERRMODE            => PDO::ERRMODE_EXCEPTION,
						PDO::ATTR_DEFAULT_FETCH_MODE => PDO::FETCH_ASSOC,
						PDO::ATTR_EMULATE_PREPARES   => false,
					];
					$passfile = fopen("secret/pass", "r") or die(header($_SERVER["SERVER_PROTOCOL"]." 418 I'm a teapot"));
					$pdo = new PDO($dsn, "mojazguba.eu", str_replace("\n", "", fgets($passfile)), $opt);
					fclose($passfile);

					$query_userdata = $pdo->prepare("SELECT id, username, password, mail FROM user WHERE username = ? or mail = ?");
					if ($query_userdata->execute(array($_POST["username"], $_POST["username"]))) {
						$userdata = $query_userdata->fetch();
					}

					if ($_POST["action"] == "1") {
						//insert into db
						if ($query_userdata->rowCount() > 0) {
							if ($userdata["username"] == $_POST["username"]) {
								$exception["USERNAME_TAKEN"] = true;
							}
							if ($userdata["mail"] == $_POST["mail"]) {
								$exception["EMAIL_TAKEN"] = true;
							}
						} else {
							if (isset($_POST["mail"])) {
								if ($_POST["mail"] != "") {//TODO: sprawdzanie adresu email filtrem
									$query_in = $pdo->prepare("INSERT INTO user (username, password, mail) VALUES (:username, :password, :mail)");
									$query_in->bindParam(':username', $_POST["username"]);
									$password_hashed = password_hash($_POST["password"], PASSWORD_DEFAULT);
									$query_in->bindParam(':password', $password_hashed);
									$query_in->bindParam(':mail', $_POST["mail"]);
									$query_in->execute();
									unset($password_hashed);
								} else {
									$exception["MISSING_EMAIL"] = true;
								}
							} else {
								$exception["MISSING_EMAIL"] = true;
							}
						}
					} else {
						//check if exist, check password
						if ($query_userdata->rowCount() > 0) {
							if (password_verify($_POST["password"], $userdata["password"])) {
								$_SESSION["username"] = $userdata["username"];
								$_SESSION["user_id"] = $userdata["id"];
							} else {
								$exception["INVALID_PASSWORD"] = true;
							}
						} else {
							$exception["MISSING_ACCOUNT"] = true;
						}
					}
				} else {
					if ($_POST["username"] == "") {
						$exception["MISSING_USERNAME"] = true;
					}
					if ($_POST["password"] == "") {
						$exception["MISSING_PASSWORD"] = true;
					}
					if ($_POST["action"] == "1") {
						if (isset($_POST["mail"])) {
							if ($_POST["mail"] == "") {
								$exception["MISSING_EMAIL"] = true;
							}
						} else {
							$exception["MISSING_EMAIL"] = true;
						}
					}
				}
			} else {
				if (!isset($_POST["username"])) {
					$exception["MISSING_USERNAME"] = true;
				}
				if (!isset($_POST["password"])) {
					$exception["MISSING_PASSWORD"] = true;
				}
			}
		}
	} else {
		$dsn = "mysql:host=localhost;dbname=mojazguba;charset=utf8mb4";
		$opt = [
			PDO::ATTR_ERRMODE            => PDO::ERRMODE_EXCEPTION,
			PDO::ATTR_DEFAULT_FETCH_MODE => PDO::FETCH_ASSOC,
			PDO::ATTR_EMULATE_PREPARES   => false,
		];
		$passfile = fopen("secret/pass", "r") or die(header($_SERVER["SERVER_PROTOCOL"]." 418 I'm a teapot"));
		$pdo = new PDO($dsn, "mojazguba.eu", str_replace("\n", "", fgets($passfile)), $opt);
		fclose($passfile);

		if ($_SERVER["REQUEST_METHOD"] == "POST") {
			if (isset($_POST["logout"])) {
				if ($_POST["logout"] == "logout") {
					session_unset();
					session_destroy();
					unset($_POST["logout"]);
					unset($_POST["username"]);
					unset($_POST["password"]);
					unset($_POST["mail"]);
				}
			}
		}
	}
?>
<!DOCTYPE html>
<html>
	<head>
		<meta charset="utf-8">
		<title>Moja zguba</title>
		<!--TODO: responsive design
		<link rel="stylesheet" media="screen and (max-device-width: 800px)" href="front/style_index_low_width.css">
		<link rel="stylesheet" media="screen and (min-device-width: 800px)" href="front/style_index.css">-->
		<link rel="stylesheet" href="front/style_index.css">
		<?php //żeby mi sie nie mylił localhost ze stroną produkcyjną xD
			if (isset($_SERVER['HTTP_HOST'])) {
				if (strpos($_SERVER['HTTP_HOST'], "mojazguba.eu") === false) {
					echo "<style>";
					echo 	"body {";
					echo 		"background-color: ";
					if ($_SERVER['HTTP_HOST'] == "localhost") {
						echo 		"magenta;";
					} else {
						echo 		"black;";
					}
					echo 	"}";
					echo "</style>";
				}
			} else {
				echo "<style>";
				echo 	"body {";
				echo 		"background-color: white";
				echo 	"}";
				echo "</style>";
			}
		?>
		<script src="front/headscript_index.js"></script>
		<script>
			<?php
				/*if (isset($exception)) {
					echo "console.log(\"exception:\");\n";
					echo "console.log(" . json_encode($exception) . ");\n";
				}
				if (isset($_SESSION)) {
					echo "console.log(\"session:\");\n";
					echo "console.log(" . json_encode($_SESSION) . ");\n";
				}
				if ($_SERVER["REQUEST_METHOD"] == "POST") {
					echo "console.log(\"post:\");\n";
					echo "console.log(" . json_encode($_POST) . ");\n";
				}*/
				if (isset($_SESSION["username"])) {
					$dev_query = $pdo->prepare("SELECT transmitter_id FROM user_transmitter WHERE owner_id = :owner_id");
					$dev_query->bindParam(':owner_id', $_SESSION["user_id"]);
					$dev_query->execute();
					if ($dev_query->rowCount() > 0) {//istnieją urządzenia
						$dev_name = "";
						$dev_time = "";
						echo "var pathCoords = [\n";
						for ($i = 0; $dev = $dev_query->fetch(); $i++) {
							$dev_name .= "	pathCoords[" . $i . "][\"id\"] = " . json_encode($dev["transmitter_id"]) . ";\n";
							$geo_query = $pdo->prepare("SELECT latitude, longtitude, localization.time FROM user_transmitter INNER JOIN localization ON user_transmitter.transmitter_id = localization.transmitter_id WHERE owner_id = ? AND localization.transmitter_id = ? AND latitude IS NOT NULL AND longtitude IS NOT NULL ORDER BY localization.time DESC");
							if ($geo_query->execute(array($_SESSION["user_id"], $dev["transmitter_id"]))) {
								if ($geo_query->rowCount() > 0) {
									echo "[";
									for ($j = 0; $geo = $geo_query->fetch(); $j++) {
										echo "{lat: " . $geo["latitude"] . ", lng: " . $geo["longtitude"] . "},";
										$dev_time .= "	pathCoords[" . $i . "][" . $j . "][\"time\"] = " . json_encode($geo["time"]) . ";\n";
									}
									echo "],\n";
								}
							}
						}
						echo "];\n";

						echo "if (pathCoords.length) {\n";
						echo "	var locations = [];\n";
						echo "	for (var i = 0; i < pathCoords.length; i++) {\n";
						echo "		locations.push(pathCoords[i][0]);\n";
						echo "	}\n";
						echo $dev_name;
						echo $dev_time;
						echo "}\n";
					}
					echo "setInterval(ajax, 15000);\n";
					echo "var layout=\"map\";";
				} else {
					echo "var layout=\"login\";";
				}
			?>
		</script>
	</head>
	<body>
		<div id="wrapper">
			<?php
				if (isset($_SESSION["username"])) {
					echo "<div id=\"map\">";
					echo "</div>";
					echo "<div id=\"menu\">";
					echo 	"<button id=\"settings\" class=\"menu_panel\">";
					echo 		"Zarządzaj";
					echo 	"</button>";
					echo 	"<button id=\"logout\" class=\"menu_panel\">";
					echo 		"Wyloguj (" . htmlspecialchars($_SESSION["username"]) . ")";
					echo 	"</button>";
					echo "</div>";
				} else {
					echo "<div id=\"menuOutline\">";
					echo 	"<div id=\"login_top\">";
					echo 		"<div id=\"logo_wrapper\">";
					echo 			"<object id=\"logo\" data=\"!Logo\logo.svg\" type=\"image/svg+xml\">";
					echo 			"</object>";
					echo 		"</div>";
					echo 		"<div id=\"logowanie\">";
					if (isset($exception["MISSING_USERNAME"])) {
						echo 		"<p class=\"exception\">";
						echo 			"Proszę podać nazwę użytkownika!";
						echo 		"</p>";
					}
					if (isset($exception["MISSING_PASSWORD"])) {
						echo 		"<p class=\"exception\">";
						echo 			"Proszę podać hasło!";
						echo 		"</p>";
					}
					if (isset($exception["INVALID_PASSWORD"])) {
						echo 		"<p class=\"exception\">";
						echo 			"Nieprawidłowe hasło!";
						echo 		"</p>";
					}
					if (isset($exception["MISSING_EMAIL"])) {
						echo 		"<p class=\"exception\">";
						echo 			"Proszę podać adres email!";
						echo 		"</p>";
					}
					if (isset($exception["MISSING_ACCOUNT"])) {
						echo 		"<p class=\"exception\">";
						echo 			"Konto nie istnieje!";
						echo 		"</p>";
					}
					if (isset($exception["USERNAME_TAKEN"])) {
						echo 		"<p class=\"exception\">";
						echo 			"Nazwa zajęta!";
						echo 		"</p>";
					}
					if (isset($exception["EMAIL_TAKEN"])) {
						echo 		"<p class=\"exception\">";
						echo 			"Adres email zajęty!";
						echo 		"</p>";
					}
					echo 			"<form id=\"login_form\" action=\"" . htmlspecialchars($_SERVER["PHP_SELF"]) . "\" method=\"post\">";
					echo 				"<input type=\"text\" id=\"input_username\" name=\"username\" maxlength=\"255\" placeholder=\"Nazwa użytkownika lub email\" ";
					if ($_SERVER["REQUEST_METHOD"] == "POST") {
						if (isset($_POST["username"])) {
							if ($_POST["username"] != "") {
								echo 		"value=\"";
								echo 		htmlspecialchars($_POST["username"]);
								echo 		"\"";
							} else {
								//ostrzeżenie
							}
						}
					}
					echo 				">";
					echo 				"<input type=\"password\" id=\"input_password\" name=\"password\" maxlength=\"255\" placeholder=\"Hasło\" ";
					if ($_SERVER["REQUEST_METHOD"] == "POST") {
						if (isset($_POST["password"])) {
							if ($_POST["password"] != "") {
								echo 		"value=\"";
								echo 		htmlspecialchars($_POST["password"]);
								echo 		"\"";
							} else {
								//ostrzeżenie
							}
						}
					}
					echo 				">";

					echo 				"<input type=\"text\" id=\"input_mail\" name=\"mail\" maxlength=\"255\" placeholder=\"Adres email\" ";
					if ($_SERVER["REQUEST_METHOD"] == "POST") {
						if (isset($_POST["mail"])) {
							if ($_POST["mail"] != "") {
								echo 		"value=\"";
								echo 		htmlspecialchars($_POST["mail"]);
								echo 		"\"";
							} else {
								//ostrzeżenie
							}
						}
					}
					echo 				">";

					echo 				"<input type=\"text\" id=\"input_action\" name=\"action\" value=\"0\" style=\"display: none\">";
					echo 			"</form>";
					echo 		"</div>";
					echo 	"</div>";
					echo 	"<div id=\"form_buttons\">";
					echo 		"<button id=\"form_switch\">";
					echo 			"Nie masz konta?";
					echo 		"</button>";
					echo 		"<button id=\"input_submit\">";
					echo 			"Zaloguj";
					echo 		"</button>";
					echo 	"</div>";
					echo "</div>";
				}
			?>
		</div>
		<script src="front/footscript_index.js"></script>
		<?php
			if (isset($_SESSION["username"])) {
				echo "<script src=\"https://developers.google.com/maps/documentation/javascript/examples/markerclusterer/markerclusterer.js\"></script>";
				echo "<script async defer src=\"https://maps.googleapis.com/maps/api/js?key=AIzaSyB6WwlUGE4aJwe2GeIN_PUuseiAK610rbA&callback=initMap\"></script>";
			}
		?>
	</body>
</html>
