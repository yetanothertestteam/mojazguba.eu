create database mojazguba;
create user 'mojazguba.eu'@'localhost' identified by /**/;
grant all on mojazguba.* to 'mojazguba.eu'@'localhost';

connect mojazguba;

create table user (
	id bigint not null auto_increment,
	username varchar(255) not null,
	password varchar(511) not null,
	mail varchar(255) not null,
	primary key (id)
);

create table transmitter (
	id char(6) not null,
	code char(6) not null,
	primary key (id)
);

create table user_transmitter (
	id bigint not null auto_increment,
	owner_id bigint not null,
	transmitter_id char(6) not null,
	primary key (id),
	foreign key (owner_id) references user(id),
	foreign key (transmitter_id) references transmitter(id)
);

create table localization (
	time datetime not null,
	transmitter_id char(6) not null,
	latitude float,
	longtitude float,
	satelites tinyint unsigned,
	latency tinyint unsigned,
	constraint localization_pk primary key (time, transmitter_id),
	foreign key (transmitter_id) references transmitter(id)
);