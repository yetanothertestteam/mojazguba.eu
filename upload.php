<?php
	if ($_SERVER["REQUEST_METHOD"] == "POST") {
		$file = fopen("logs/upload_posts", "a");
        fwrite($file, "[" . date("c") . "] " . json_encode($_POST) . "\n");
		if (isset($_POST["time"]) && isset($_POST["id"]) && isset($_POST["lat"]) && isset($_POST["long"])) {
			if ($_POST["time"] != "" && $_POST["id"] != "" && $_POST["lat"] != "" && $_POST["long"] != "") {
				if (preg_match("/^\d{2}:\d{2}$/", $_POST["time"]) && 
				(preg_match("/^[-]{0,1}\d+.\d+$/", $_POST["lat"]) || $_POST["lat"] == "null") && 
				(preg_match("/^[-]{0,1}\d+.\d+$/", $_POST["long"]) || $_POST["long"] == "null") && 
				preg_match("/^[A-z0-9+\/-_=]{6}$/", $_POST["id"])) {
					$dsn = "mysql:host=localhost;dbname=mojazguba;charset=utf8mb4";
					$opt = [
						PDO::ATTR_ERRMODE            => PDO::ERRMODE_EXCEPTION,
						PDO::ATTR_DEFAULT_FETCH_MODE => PDO::FETCH_ASSOC,
						PDO::ATTR_EMULATE_PREPARES   => false,
					];
					$passfile = fopen("secret/pass", "r") or die(header($_SERVER["SERVER_PROTOCOL"] . " 418 I'm a teapot"));
					$upload = new PDO($dsn, "mojazguba.eu", str_replace("\n", "", fgets($passfile)), $opt);
					fclose($passfile);
					
					$pre_query = $upload->prepare("SELECT * FROM transmitter WHERE id = ?");
					$pre_query->execute(array($_POST["id"]));
					$transmitter = $pre_query->fetch();
					if ($transmitter != null) {
						$now = date("c");
						$query = $upload->prepare("INSERT INTO localization VALUES (:time, :transmitter_id, :latitude, :longtitude, :satelites, :latency)");
						$time = str_replace("T", " ", explode(":", $now)[0] . ":" . $_POST["time"]);
						$query->bindParam(':time', $time);
						$query->bindParam(':transmitter_id', $_POST["id"]);
						$query->bindParam(':latitude', $_POST["lat"]);
						$query->bindParam(':longtitude', $_POST["long"]);
						if (isset($_POST["sat"])) {
							if (is_numeric($_POST["sat"])) {
								$query->bindParam(':satelites', $_POST["sat"]);
							} else {
								$query->bindParam(':satelites', null);
							}
						} else {
							$query->bindParam(':satelites', null);
						}
						$laten = (explode(":", $now)[1] * 60 + intval(explode(":", $now)[2])) - (explode(":", $_POST["time"])[0] * 60 + explode(":", $_POST["time"])[1]);
						$query->bindParam(':latency', $laten);
						try {
							$query->execute();
						} catch (PDOException $exception) { //Fatal error: Uncaught PDOException: SQLSTATE[23000]: Integrity constraint violation: 1062 Duplicate entry '20:05:45-qwerty' for key 'PRIMARY'
							if ($exception->getCode() == 23000) {
								if (strpos($exception->getMessage(), "1062 Duplicate entry")) {
									header($_SERVER["SERVER_PROTOCOL"]." 409 Conflict");
									fwrite($file, "    ^409\n");
									fclose($file);
									die();
								}
							}
						}
						$post_query = $upload->prepare("SELECT * FROM localization WHERE time = ? and transmitter_id = ?");
						$post_query->execute(array($time, $_POST["id"]));
						$check = $post_query->fetch();
						if ($check != null) {
							header($_SERVER["SERVER_PROTOCOL"]." 200 Ok");
							echo "Ok";
							fwrite($file, "    ^200\n");
							fclose($file);
							//deleting old data
							$cleanup_query = $upload->prepare("DELETE FROM localization WHERE time < (NOW() - INTERVAL 1 DAY) AND transmitter_id = :transmitter_id");
							$cleanup_query->bindParam(':transmitter_id', $_POST["id"]);
							$cleanup_query->execute();
						} else {
							header($_SERVER["SERVER_PROTOCOL"]." 500 Internal Server Error");
							fwrite($file, "    ^500\n");
							fclose($file);
							die();
						}
					} else {
						header($_SERVER["SERVER_PROTOCOL"]." 406 Not Acceptable");
						fwrite($file, "    ^406\n");
						fclose($file);
						die();
					}
				} else {
					header($_SERVER["SERVER_PROTOCOL"]." 400 Bad Request");
					fwrite($file, "    ^400\n");
					fclose($file);
					die();
				}
			} else {
				header($_SERVER["SERVER_PROTOCOL"]." 400 Bad Request");
				fwrite($file, "    ^400\n");
				fclose($file);
				die();
			}
		} else {
			header($_SERVER["SERVER_PROTOCOL"]." 400 Bad Request");
			fwrite($file, "    ^400\n");
			fclose($file);
			die();
		}
	} else {
		$file = fopen("logs/upload_bad_requests", "a");
		if ($_SERVER["REQUEST_METHOD"] == "GET") {
			if ($_GET != null) {
				fwrite($file, "[" . date("c") . "] " . json_encode($_GET) . "\n");
			} else {
				fwrite($file, "[" . date("c") . "] GET no data\n");
			}
		} else {
			fwrite($file, "[" . date("c") . "] non-POST non-GET\n");
		}
		header($_SERVER["SERVER_PROTOCOL"]." 400 Bad Request");
		fwrite($file, "    ^400\n");
        fclose($file);
		die();
	}
?>