<?php
	/* 	PSEUDOWYJĄTKI UŻYTE W KODZIE ($exception):
		"MISSING_USERNAME" - niepodana nazwa użytkownika //do odsiania przez js
		"MISSING_PASSWORD" - niepodane hasło //do odsiania przez js
		"INVALID_PASSWORD" - nieprawidłowe hasło (logowanie)
		"MISSING_EMAIL" - niepodany adres email //do odsiania przez js
		"MISSING_ACCOUNT" - konto z podaną nazwą użytkownika / adresem email nie istnieje (logowanie)
		"USERNAME_TAKEN" - nazwa użytkownika już zajęta (rejestracja)
		"EMAIL_TAKEN" - adres email już zajęty (rejestracja)
	*/
	header("Cache-Control: no-cache");
	header("Pragma: no-cache");
	session_start();
	if (isset($_SESSION["username"])) {
		$dsn = "mysql:host=localhost;dbname=mojazguba;charset=utf8mb4";
		$opt = [
			PDO::ATTR_ERRMODE            => PDO::ERRMODE_EXCEPTION,
			PDO::ATTR_DEFAULT_FETCH_MODE => PDO::FETCH_ASSOC,
			PDO::ATTR_EMULATE_PREPARES   => false,
		];
		$passfile = fopen("secret/pass", "r") or die(header($_SERVER["SERVER_PROTOCOL"]." 418 I'm a teapot"));
		$pdo = new PDO($dsn, "mojazguba.eu", str_replace("\n", "", fgets($passfile)), $opt);
		fclose($passfile);
		
		$dev_query = $pdo->prepare("SELECT transmitter_id FROM user_transmitter WHERE owner_id = :owner_id");
		$dev_query->bindParam(':owner_id', $_SESSION["user_id"]);
		$dev_query->execute();
		if ($dev_query->rowCount() > 0) {//istnieją urządzenia
			$pathCoords = array();
			$singlePath = array();
			$locations = array();
			$ids = array();
			for ($i = 0; $dev = $dev_query->fetch(); $i++) {
				$geo_query = $pdo->prepare("SELECT latitude, longtitude, localization.time FROM user_transmitter INNER JOIN localization ON user_transmitter.transmitter_id = localization.transmitter_id WHERE owner_id = ? AND localization.transmitter_id = ? AND latitude IS NOT NULL AND longtitude IS NOT NULL ORDER BY localization.time DESC");
				if ($geo_query->execute(array($_SESSION["user_id"], $dev["transmitter_id"]))) {
					for ($j = 0; $geo = $geo_query->fetch(); $j++) {
						$tmp = array("lat" => $geo["latitude"], "lng" => $geo["longtitude"], "time" => $geo["time"]);
						array_push($singlePath, $tmp);
						if ($j == 0) {
							array_push($locations, $tmp);
						}
					}
					//$singlePath["id"] = $dev["transmitter_id"];
					array_push($ids, $dev["transmitter_id"]);
					array_push($pathCoords, $singlePath);
					$singlePath = array();
					
				}
			}
			echo json_encode(array($pathCoords, $locations, $ids));
		}
	} else {
		header("Location: index.php");
		die();
	}
?>