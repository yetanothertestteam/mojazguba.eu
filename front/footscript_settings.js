if (document.getElementById("logout") != null) {
	document.getElementById("logout").onclick = function() {//by Rakesh Pai: https://stackoverflow.com/a/133997
		// The rest of this code assumes you are not using a library.
		// It can be made less wordy if you use one.
		var form = document.createElement("form");
		form.setAttribute("method", "post");
		form.setAttribute("action", "index.php");

		var hiddenField = document.createElement("input");
		hiddenField.setAttribute("type", "hidden");
		hiddenField.setAttribute("name", "logout");
		hiddenField.setAttribute("value", "logout");

		form.appendChild(hiddenField);

		document.body.appendChild(form);
		form.submit();
	};
} else {
	console.log("Nie znaleziono elementu logout");
}

if (document.getElementById("back") != null) {
	document.getElementById("back").onclick = function() {window.location.assign("index.php");};
} else {
	console.log("Nie znaleziono elementu back");
}

if (document.getElementsByClassName("delete_button") != null) {
	var a = document.getElementsByClassName("delete_button");
	for (var i = 0; i < a.length; i++) {
		a[i].onclick = function() {//by Rakesh Pai: https://stackoverflow.com/a/133997
			// The rest of this code assumes you are not using a library.
			// It can be made less wordy if you use one.
			var form = document.createElement("form");
			form.setAttribute("method", "post");
			form.setAttribute("action", "settings.php");

			var hiddenField = document.createElement("input");
			hiddenField.setAttribute("type", "hidden");
			hiddenField.setAttribute("name", "id");
			hiddenField.setAttribute("value", this.id);

			var hiddenField2 = document.createElement("input");
			hiddenField2.setAttribute("type", "hidden");
			hiddenField2.setAttribute("name", "action");
			hiddenField2.setAttribute("value", "0");

			form.appendChild(hiddenField);
			form.appendChild(hiddenField2);

			document.body.appendChild(form);
			form.submit();
		};
	}
}