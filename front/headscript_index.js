var map;
var markers = [];
var pathMarkers = [];
var outPath = [];
var path = [];
var response;
var locations = [];
var pathCoords = [];

function switchForm(type) {
	switch (type) {
		case 1: {
			document.getElementById("form_switch").onclick = function() {switchForm(0);};
			document.getElementById("form_switch").innerHTML = "Masz już konto?";
			document.getElementById("input_submit").innerHTML = "Zarejestruj";
			document.getElementById("input_action").value = "1";
			document.getElementById("input_username").placeholder = "Nazwa użytkownika";
			document.getElementById("input_mail").style.visibility = "visible";
			break;
		}
		default: {
			document.getElementById("form_switch").onclick = function() {switchForm(1);};
			document.getElementById("form_switch").innerHTML = "Nie masz konta?";
			document.getElementById("input_submit").innerHTML = "Zaloguj";
			document.getElementById("input_action").value = "0";
			document.getElementById("input_username").placeholder = "Nazwa użytkownika /email";
			document.getElementById("input_mail").style.visibility = "hidden";
		}
	}
}

function clearForm() {
	switchForm(0);
	document.getElementById("input_username").value = "";
	document.getElementById("input_password").value = "";
}

function ajax() {
	var xmlhttp = new XMLHttpRequest();
	xmlhttp.onreadystatechange = function() {
		if (this.readyState == 4 && this.status == 200) {
			response = this.responseText;
			if (response != "") {
				locations.length = 0;
				//console.log(JSON.parse(this.responseText));
				locations = JSON.parse(this.responseText)[1];
				pathCoords = JSON.parse(this.responseText)[0];
				JSON.parse(this.responseText)[2].forEach(function(element, index, array) {pathCoords[index]["id"] = element;});
				markers.forEach(function(element, index, array) {element.setMap(null)});
				markers.length = 0;
				pathMarkers.forEach(function(element, index, array) {element.setMap(null)});
				pathMarkers.length = 0;
				outPath.forEach(function(element, index, array) {element.setMap(null)});
				outPath.length = 0;
				path.forEach(function(element, index, array) {element.setMap(null)});
				path.length = 0;
				initMapMarkers();
			}
		}
	};
	xmlhttp.open("POST", "ajax.php");
	xmlhttp.setRequestHeader("Content-type", "application/json");
	xmlhttp.send();
}
