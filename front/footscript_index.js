if (document.getElementById("form_switch") != null) {
	document.getElementById("form_switch").onclick = function() {switchForm(1);};
} else {
	if (layout === "login") {
		console.log("Nie znaleziono elementu form_switch");
	}
}
if (document.getElementById("input_submit") != null) {
	document.getElementById("input_submit").onclick = function() {document.getElementById("login_form").submit();};
} else {
	if (layout === "login") {
		console.log("Nie znaleziono elementu input_submit");
	}
}
if (document.getElementById("logout") != null) {
	document.getElementById("logout").onclick = function() {//by Rakesh Pai: https://stackoverflow.com/a/133997
		// The rest of this code assumes you are not using a library.
		// It can be made less wordy if you use one.
		var form = document.createElement("form");
		form.setAttribute("method", "post");

		var hiddenField = document.createElement("input");
		hiddenField.setAttribute("type", "hidden");
		hiddenField.setAttribute("name", "logout");
		hiddenField.setAttribute("value", "logout");

		form.appendChild(hiddenField);

		document.body.appendChild(form);
		form.submit();
	};
} else {
	if (layout === "map") {
		console.log("Nie znaleziono elementu logout");
	}
}
if (document.getElementById("settings") != null) {
	document.getElementById("settings").onclick = function() {window.location.assign("settings.php");};
} else {
	if (layout === "map") {
		console.log("Nie znaleziono elementu settings");
	}
}
if (document.getElementById("input_username") != null) {
	document.getElementById("input_username").onkeydown = function () {
		if (event.which || event.keyCode) {
			if ((event.which == 13) || (event.keyCode == 13)) {
				document.getElementById("login_form").submit();
			}
		}
	};
} else {
	if (layout === "login") {
		console.log("Nie znaleziono elementu login_username");
	}
}
if (document.getElementById("input_password") != null) {
	document.getElementById("input_password").onkeydown = function () {
		if (event.which || event.keyCode) {
			if ((event.which == 13) || (event.keyCode == 13)) {
				document.getElementById("login_form").submit();
			}
		}
	};
} else {
	if (layout === "login") {
		console.log("Nie znaleziono elementu input_password");
	}
}
if (document.getElementById("input_mail") != null) {
	document.getElementById("input_mail").onkeydown = function () {
		if (event.which || event.keyCode) {
			if ((event.which == 13) || (event.keyCode == 13)) {
				document.getElementById("login_form").submit();
			}
		}
	};
} else {
	if (layout === "login") {
		console.log("Nie znaleziono elementu input_mail");
	}
}

function initMap() {
	map = new google.maps.Map(document.getElementById('map'), {
		zoom: pathCoords.length ? 9 : 5,
		center: pathCoords.length ? pathCoords[0][0] : {lat: 52.229, lng: 21.011},
	});

	initMapMarkers();

	if (markers.length) {
		var bounds = new google.maps.LatLngBounds();
		for (var i = 0; i < markers.length; i++) {
			bounds.extend(markers[i].getPosition());
		}
		map.fitBounds(bounds);
		google.maps.event.addListenerOnce(map, 'idle', function() {if (map.getZoom() > 19) {map.setZoom(19);}});
	}
}

function initMapMarkers() {
	// Create an array of alphabetical characters used to label the markers.
	var colors = ["98FB98", "F0F8FF", "FFE4C4", "FF4500", "FFA500", "00BFFF", "000080", "FF6347", "A9A9A9", "FFFFFF", "7FFFD4", "FF69B4", "FF00FF", "800000", "ADFF2F", "8FBC8F", "FAEBD7", "AFEEEE", "BDB76B", "B8860B", "4B0082", "FF7F50", "DCDCDC", "DDA0DD", "DEB887", "E9967A", "CD853F", "5F9EA0", "D2691E"];

	zoom = map.getZoom();

	if (pathCoords.length) {
		if (pathCoords.length) {
			pathCoords.forEach(function(element, index, array) {
				outPath.push(new google.maps.Polyline({
					path: element,
					geodesic: true,
					strokeColor: "#000000",
					strokeOpacity: 0.75,
					strokeWeight: 4.5,
					map: map,
					visible: zoom > 4
				}));
			});

			pathCoords.forEach(function(element, index, array) {
				path.push(new google.maps.Polyline({
					path: element,
					geodesic: true,
					strokeColor: "#" + colors[index % colors.length],
					strokeOpacity: 1.0,
					strokeWeight: 2,
					map: map,
					visible: zoom > 4
				}));
			});

			map.addListener('zoom_changed', function() {
				var zoom = map.getZoom();
				// iterate over markers and call setVisible
				outPath.forEach(function(element, index, array) {element.setVisible(zoom > 4);});
				path.forEach(function(element, index, array) {element.setVisible(zoom > 4);});
			});
		}
	}

	// Add some markers to the map.
	// Note: The code uses the JavaScript Array.prototype.map() method to
	// create an array of markers based on a given "locations" array.
	// The map() method here has nothing to do with the Google Maps API.
	if (locations.length) {
		//console.log(locations.length);
		pathCoords.forEach(function(ELEMENT, INDEX, ARRAY) {
			ELEMENT.forEach(function(element, index, array) {
				if (index > 0) {
					pathMarkers.push(new google.maps.Marker({
						position: element,
						icon: {
							path: 'M -8,0 0,-8 8,0 0,8 z',
							strokeColor: '#000',
							fillColor: "#" + colors[INDEX % colors.length],
							fillOpacity: 1,
							strokeWeight: 1.25
						},
						title: array["id"] + "\n" + element["time"],
						map: map,
						visible: zoom > 7
					}));
					//console.log(pathMarker);
				}
			});
		});

		locations.forEach(function(element, index, array) {
			markers.push(new google.maps.Marker({
				position: element,
				icon: "https://chart.apis.google.com/chart?chst=d_map_pin_letter&chld=%E2%80%A2|" + colors[index % colors.length],
				title: pathCoords[index]["id"] + "\n" + locations[index]["time"],
				map: map
			}));
		});

		map.addListener('zoom_changed', function() {
			var zoom = map.getZoom();
			// iterate over markers and call setVisible
			pathMarkers.forEach(function(element, index, array) {element.setVisible(zoom > 7);});
		});
	}
}
