function switchForm(type) {
	switch (type) {
		case 1: {
			document.getElementById("form_switch").onclick = function() {switchForm(0);};
			document.getElementById("form_switch").innerHTML = "Masz już konto?";
			document.getElementById("input_submit").innerHTML = "Zarejestruj";
			document.getElementById("input_action").value = "1";
			document.getElementById("input_username").placeholder = "Nazwa użytkownika";
			document.getElementById("input_mail").style.visibility = "visible";
			break;
		}
		default: {
			document.getElementById("form_switch").onclick = function() {switchForm(1);};
			document.getElementById("form_switch").innerHTML = "Nie masz konta?";
			document.getElementById("input_submit").innerHTML = "Zaloguj";
			document.getElementById("input_action").value = "0";
			document.getElementById("input_username").placeholder = "Nazwa użytkownika /email";
			document.getElementById("input_mail").style.visibility = "hidden";
		}
	}
}

function clearForm() {
	switchForm(0);
	document.getElementById("input_username").value = "";
	document.getElementById("input_password").value = "";
}