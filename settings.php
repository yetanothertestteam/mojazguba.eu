<?php
	/* 	PSEUDOWYJĄTKI UŻYTE W KODZIE ($exception):
		"MISSING_ID" - niepodany identyfiktor nadajnika //do odsiania przez js
		"INVALID_ID" - nieprawidłowy identyfiktor nadajnika
		"MISSING_CODE" - niepodany kod autoryzacji klienta nadajnika //do odsiania przez js
		"INVALID_CODE" - nieprawidłowy kod autoryzacji klienta nadajnika
		"ALREADY_BOUND" - próba zdublowania wpisu w tabeli user_transmitter (istniejące już powiązanie)
		"MISSING_BINDING" - próba usunięcia nieistniejącego powiązania (nieistniejące powiązanie; fizycznie niemożliwe do wystąpienia)
	*/
	header("Cache-Control: no-cache");
	header("Pragma: no-cache");
	session_start();
	if (isset($_SESSION["username"])) {
		$dsn = "mysql:host=localhost;dbname=mojazguba;charset=utf8mb4";
		$opt = [
			PDO::ATTR_ERRMODE            => PDO::ERRMODE_EXCEPTION,
			PDO::ATTR_DEFAULT_FETCH_MODE => PDO::FETCH_ASSOC,
			PDO::ATTR_EMULATE_PREPARES   => false,
		];
		$passfile = fopen("secret/pass", "r") or die(header($_SERVER["SERVER_PROTOCOL"]." 418 I'm a teapot"));
		$pdo = new PDO($dsn, "mojazguba.eu", str_replace("\n", "", fgets($passfile)), $opt);
		fclose($passfile);
		
		if ($_SERVER["REQUEST_METHOD"] == "POST") {
			if (isset($_POST["id"]) && isset($_POST["action"])) {
				if ($_POST["id"] != "" && $_POST["action"] != "") {

					$query_tx = $pdo->prepare("SELECT id, code FROM transmitter WHERE id = ?");
					if ($query_tx->execute(array($_POST["id"]))) {
						$tx = $query_tx->fetch();
					}
					
					if ($query_tx->rowCount() > 0) {
						//transmitter exists
						$query_out = $pdo->prepare("SELECT owner_id, transmitter_id FROM user_transmitter WHERE owner_id = ? AND transmitter_id = ?");
						if ($query_out->execute(array($_SESSION["user_id"], $_POST["id"]))) {
							$bindings = $query_out->fetch();
						}
						if ($_POST["action"] == "1") {
							//insert into db
							if ($query_out->rowCount() > 0) {
								$exception["ALREADY_BOUND"] = true;
							} else {
								if (isset($_POST["code"])) {
									if ($tx["code"] == $_POST["code"]) {
										$query_in = $pdo->prepare("INSERT INTO user_transmitter VALUES (NULL, :owner, :transmitter)");
										$query_in->bindParam(':owner', $_SESSION["user_id"]);
										$query_in->bindParam(':transmitter', $_POST["id"]);
										$query_in->execute();
									} else {
										$exception["INVALID_CODE"] = true;
									}
								} else {
									$exception["MISSING_CODE"] = true;
								}
							}
						} else {
							//delete from db
							if ($query_out->rowCount() > 0) {
								$query_in = $pdo->prepare("DELETE FROM user_transmitter WHERE owner_id = :owner AND transmitter_id = :transmitter");
								$query_in->bindParam(':owner', $_SESSION["user_id"]);
								$query_in->bindParam(':transmitter', $_POST["id"]);
								$query_in->execute();
							} else {
								$exception["MISSING_BINDING"] = true;
							}
						}
					} else {
						//transmitter doesn't exist
						$exception["INVALID_ID"] = true;
					}
				} else {
					$exception["INVALID_ID"] = true;
				}
			} else {
				$exception["MISSING_ID"] = true;
			}
		}
		$query_devices = $pdo->prepare("SELECT transmitter.id FROM user INNER JOIN user_transmitter ON user.id = user_transmitter.owner_id INNER JOIN transmitter ON user_transmitter.transmitter_id = transmitter.id WHERE username = ?");
		$query_times = $pdo->prepare("SELECT time FROM localization WHERE transmitter_id = ? AND time >= (NOW() - INTERVAL 1 MINUTE) ORDER BY time DESC LIMIT 1");
	} else {
		header("Location: index.php");
		die();
	}
?>
<!DOCTYPE html>
<html>
	<head>
		<meta charset="utf-8">
		<title>Moja zguba</title>
		<link rel="stylesheet" href="front/style_settings.css">
		<?php //żeby mi sie nie mylił localhost ze stroną produkcyjną xD
			if (isset($_SERVER['HTTP_HOST'])) {
				if (strpos($_SERVER['HTTP_HOST'], "mojazguba.eu") === false) {
					echo "<style>";
					echo 	"body {";
					echo 		"background-color: ";
					if ($_SERVER['HTTP_HOST'] == "localhost") {
						echo 		"magenta;";
					} else {
						echo 		"black;";
					}
					echo 	"}";
					echo "</style>";
				}
			} else {
				echo "<style>";
				echo 	"body {";
				echo 		"background-color: white";
				echo 	"}";
				echo "</style>";
			}
		?>
		<script src="front/headscript_settings.js"></script>
		<script>
			<?php
				if (isset($exception)) {
					echo "console.log(" . json_encode($exception) . ");";
				}
				if ($_SERVER["REQUEST_METHOD"] == "POST") {
					echo "console.log(" . json_encode($_POST) . ");";
				}
			?>
		</script>
	</head>
	<body>
		<div id="wrapper">
			<div id="outline">
				<?php
					if (isset($exception["MISSING_ID"])) {
						echo "<p class=\"exception\">";
						echo 	"Proszę podać ID nadajnika!";
						echo "</p>";
					}
					if (isset($exception["INVALID_ID"])) {
						echo "<p class=\"exception\">";
						echo 	"Nieprawidłowy identyfikator nadajnika!";
						echo "</p>";
					}
					if (isset($exception["MISSING_CODE"])) {
						echo "<p class=\"exception\">";
						echo 	"Proszę podać kod dostarczony z nadajnikiem!";
						echo "</p>";
					}
					if (isset($exception["INVALID_CODE"])) {
						echo "<p class=\"exception\">";
						echo 	"Nieprawidłowy kod nadajnika!";
						echo "</p>";
					}
					if (isset($exception["ALREADY_BOUND"])) {
						echo "<p class=\"exception\">";
						echo 	"Nadajnik jest już powiązany z tym kontem!";
						echo "</p>";
					}
					if (isset($exception["MISSING_BINDING"])) {
						echo "<p class=\"exception\">";
						echo 	"Zabawa kodem strony jest w pełni legalna, prosimy jednak o powiadomienie nas o wszelkich wykrytych niedoskonałościach ";
						echo 	"<a href=\"mailto:dawidsygocki1234@wp.pl?subject=Luki w zabezpieczaniach&cc=kuba@towarek.pl\">";
						echo 		"drogą mailową";
						echo 	"</a>";
						echo "</p>";
					}
					echo 	"<table>";
					echo 		"<caption>";
					if ($query_devices->execute(array($_SESSION["username"]))) {
						if ($query_devices->rowCount() > 0) {
							echo 	"Z kontem powiązan";
							if ($query_devices->rowCount() == 1) {
								echo 	"e jest 1 urządzenie:";
							} else if ($query_devices->rowCount() < 5) {
								echo 	"e są " . $query_devices->rowCount() . " urządzenia:";
							} else {
								echo 	"ych jest " . $query_devices->rowCount() . " urządzeń:";
							}
							echo "</caption>";
							while ($device = $query_devices->fetch()) {
								echo "<tr>";
								echo 	"<td class=\"active\">";
								if ($query_times->execute(array($device["id"]))) {
									if ($query_times->rowCount() > 0) {
										echo "<img src=\"img/settings_greendot.svg\">";
									} else {
										echo "<img src=\"img/settings_reddot.svg\">";
									}
								}
								echo 	"</td>";
								echo 	"<td class=\"device\">";
								echo 		$device["id"];
								echo 	"</td>";
								//echo czy aktywne
								echo 	"<td class=\"button\">";
								echo 		"<img id=\"" . $device["id"] . "\" class=\"delete_button\" src=\"img/settings_cross.svg\">";
								echo 	"</td>";
								echo "</tr>";
							}
						} else {
							echo 	"Brak powiązanych nadajników!";
							echo "</caption>";
						}
						echo "</table>";
					}
					
					echo "<form id=\"addition_form\" action=\"" . htmlspecialchars($_SERVER["PHP_SELF"]) . "\" method=\"post\">";
					echo 	"<input type=\"text\" id=\"input_id\" name=\"id\" maxlength=\"6\" placeholder=\"ID\">";
					echo 	"<input type=\"text\" id=\"input_code\" name=\"code\" maxlength=\"6\" placeholder=\"Kod\">";
					echo 	"<input type=\"hidden\" id=\"input_action\" name=\"action\" value=\"1\">";
					echo 	"<input type=\"submit\" id=\"submit_button\" value=\"\">";
					echo "</form>";
				
					echo "<div id=\"menu\">";
					echo 	"<button id=\"back\" class=\"menu_panel\">";
					echo 		"Śledź";
					echo 	"</button>";
					echo 	"<button id=\"logout\" class=\"menu_panel\">";
					echo 		"Wyloguj (" . htmlspecialchars($_SESSION["username"]) . ")";
					echo 	"</button>";
					echo "</div>";
				?>
			</div>
		</div>
		<script src="front/footscript_settings.js"></script>
	</body>
</html>
